# La France vous a à l'œil !

		Tout ce que vous faites est surveillé par l'État, des appels téléphoniques aux retraits bancaires -- tout est stocké, traité, analysé.

## Surveiller les Français

Dans cet article nous parlerons de la surveillance numérique étatique – qui émane de l’État – mais avant de commencer, pour une meilleure appréhension, place aux définitions.

La surveillance consiste tout d’abord à observer attentivement une personne, un objet ou un ensemble pour le contrôler. Cependant elle renvoie aussi à la notion d’attention et de protection.

Le numérique, lui, relève du domaine des nombres et est à opposer aux données analogiques.

Ainsi, la surveillance numérique étatique n’est autre que la prise d’informations informatisées organisée par l’État.

		--

Mais depuis quand sommes-nous surveillés ? En 1978, la loi n°78-17 du 6 janvier a été la première traitant de la protection des données informatiques. Tous les ans, elle est consolidée par de nouvelles lois.

La commission Européenne a abrogé en 2014, pour attaque fondamentale à la vie privée, la « directive sur la conservation des données personnelles » mise en vigueur le 15 mars 2006.

La loi relative au renseignement de 2015 contient l’article L. 851-4 qui a pour but l’installation de boites noires chez les FAI (cf. Glossaire, page 3), permettant ainsi à l’État d’espionner légalement les communications entre les particuliers, les entreprises, et cætera.

Le mardi 18 juillet 2017, le Sénat a adopté le projet de loi renforçant la sécurité intérieure et la lutte contre le terrorisme. Cette loi a comme but de banaliser l’état d’urgence dans la loi, au niveau de la surveillance.

## Big Brother

Depuis 2013, on sait que le gouvernement utilise un autre logiciel, bien plus puissant, pour récolter et traiter les données Big Data en France. Il est opéré par la DGSE (cf. Glossaire, page 3) grâce au supercalculateur situé boulevard Mortier à Paris.

Les données récoltées sont principalement utilisées par les agences gouvernementales, parmi elles, la DNRED, la DPSD, la DRM, la DCRI, Tracfin et la préfecture de police de Paris (cf. Glossaire, page 3).

Ces métadonnées sont récoltées depuis les Plateformes Web et tous types d’applications (du portable à l’ordinateur, en passant par la tablette à la TV connectée) fournies par Google, Yahoo, Microsoft, Apple, AOP, YouTube, Skype, Paltalk ou Facebook et bien d’autres encore ! 

Nous ne sommes pas les seuls à employer ce genre de techniques, les services secrets États-Uniens utilisent Prism, alors que la Libye, le Gabon, le Qatar, l'Arabie saoudite, le Kazakhstan, Dubaï et le Maroc utilisent Eagle.

## Surveillance généralisée

Pourcentage de la population Française connectée
 - 83%	sont des internautes
 - 68%	sont sur les réseaux sociaux
 - 42%	sont sur FB

En 2009, la France déploie sa première surveillance numérique ou cyber-surveillance généralisée. Ce premier dispositif est nommé IOL (Interception Obligatoires Légales), il permet à l’État de surveiller la population à travers le réseau ADSL en traitant les métadonnées ou BigData. Pour cela, il se sert du logiciel Eagle (données personnelles).

Interceptions de sécurité en 2013
 - 6100	Acceptées
 - 82	Refusées

## Pourquoi observer ?

Téléphonie mobile
 - 0.28	Milliards d'abonnés en 1994
 - 78.4	Milliards en 2014
 - 72.1 Milliards en 2015
 - 202	Milliards de SMS en 2016

La surveillance permet de contrôler grâce à l’observation.

Son principal avantage est de prévenir les menaces potentielles. Vous êtes sous surveillance pour éviter des guerres potentielles, ou pour vous protéger des attaques terroristes éventuelles. L’État peut même prévenir des attaques sur le plan économique. 

Malgré tout, cette ‘sécurité’ a un prix. Il y a un détournement d’usage de vos données récupérées lors d’une intrusion dans votre vie privée. Et sans même que vous ne le sachiez, un tri social est effectué grâce à vos informations.

Motifs des interventions de sécurité
 - 54%	Criminalité
 - 28%	Terrorisme
 - 17%	Sécurité Nationale
 - 2%	Autres
